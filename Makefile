# the command needed to invoke Java 11
JAVA11=java11
# this variable intentionally empty by default
GRAMMAR-NAME=

.PHONY: run clean

# common tasks

lib:
	mkdir -p lib

lib/antlr-4.13.0-complete.jar: lib
	curl https://www.antlr.org/download/antlr-4.13.0-complete.jar --output lib/antlr-4.13.0-complete.jar

setup: lib/antlr-4.13.0-complete.jar

# java tasks

antlr4-java: setup
ifeq ($(GRAMMAR-NAME),)
	@echo "ERROR: Unable to generate Java parser: grammar name not set."
	@echo "Add argument GRAMMAR-NAME=<name> to your Make command where <name> is the name of the grammar."
else
	@echo $(GRAMMAR-NAME)
	$(JAVA11) -Xmx500M -cp "lib/antlr-4.13.0-complete.jar" org.antlr.v4.Tool grammars/$(GRAMMAR-NAME).g4 -o target/java/$(GRAMMAR-NAME)
endif

# python tasks
# inspired by https://earthly.dev/blog/python-makefile/

venv/bin/activate: requirements.txt
	python3 -m venv venv
	./venv/bin/pip install -r requirements.txt

antlr4-python3: venv/bin/activate
	$(JAVA11) -Xmx500M -cp "lib/antlr-4.13.0-complete.jar" org.antlr.v4.Tool grammars/$(GRAMMAR-NAME).g4 -Dlanguage=Python3 -o target/python/$(GRAMMAR-NAME)

python3-clean:
	rm -rf venv