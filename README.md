# ANTLR Sandbox

A project for experimenting with ANTLR for parser generation.

Currently, the project is set up for generating parsers using [ANTLR v4](https://github.com/antlr/antlr4). The targets currently supported by this project are:

* Java
* Python 3

See the [Usage](#usage) section for more details.

This project does not currently publish the generated parsers anywhere - it only outputs the parser contents. If you want to use the generated parser, you will need to add publishing logic (e.g. Maven or PyPI) to this project or manually copy the generated parser files into another project.

## Status

The contents of this repository exist primarily for my own development purposes and reference. It is available publically as a resource for anyone wants to structure their own setup after mine or see how working with ANTLR can be done locally.

## Requirements

These items need to be installed for this project to work correctly. This project does not handle the installation of these dependencies.

- Java 11: necessary for running ANTLR4
- GNU Make:
    - This project was tested on version 4.1, but should work for any recent version.
    - You can technically run this project without Make by running the associated Make commands in your Terminal.

Optional dependencies:

- curl: necessary for downloading the ANTLR JAR
    - The ``curl`` dependency can be bypassed by downloading the ANTLR JAR some other way. See the [Makefile](./Makefile) for the file that is needed and where it should be placed.
- Python3: only necessary if using Python3 as a target

## Installation

First, ensure that you have the required dependencies described above.

Then clone this repository to your local workstation.

    $ git clone https://gitlab.com/jamestarka/antlr-sandbox.git

Next, configure the Java 11 runtime location. The [Makefile](./Makefile) used to run commands in this project assumes that a Java 11 runtime is available using the ``java11`` command. You can set an alias for ``java11`` (e.g. in your ``.bashrc`` file) like this:

```
alias java11=<path-to-java11>/bin/java
```

Alternatively, you can specify the Java11 location on a per-command basis by passing the ``JAVA11`` variable into the Make command. For example, to use the Java 11 installation at ``/usr/lib/java11``, the command would look something like this:

```
make antlr4-java GRAMMAR-NAME=foo JAVA11=/usr/lib/java11/bin/java
```

Finally, you can edit the [Makefile](./Makefile) itself to change the value of the ``JAVA11`` variable.

## Usage

First, create the grammar file that describes your grammar. Alternatively, you can download [one of the pre-defined ANTLR4 grammars](https://github.com/antlr/grammars-v4). The grammar file can be placed in a directory called ``grammars`` located at the root level of this repository. The name of the grammar file should not contain any spaces and should end in ``.g4``.

Then follow the steps below to generate a parser for one of the target languages.

### Generate Java Parser

To generate a Java parser for your grammar, run the following command:

    $ make antlr4-java GRAMMAR-NAME=<grammar>

Replace ``<grammar>`` with the name of your grammar. The grammar name should match the name of the grammar file, without the ``.g4`` extension. For example, to generate a Java parser using the grammar in ``grammars/foo.g4``, the command would be:

    $ make antlr4-java GRAMMAR-NAME=foo

The generated parser files will be put into the directory ``target/java/<grammar-name>``. Any previous files with the same name as the generated parser files will be overwritten.

### Generate Python 3 Parser

To generate a Python 3 parser for your grammar, run the following command:

    $ make antlr4-python3 GRAMMAR-NAME=<grammar>

Replace ``<grammar>`` with the name of your grammar. The grammar name should match the name of the grammar file, without the ``.g4`` extension. For example, to generate a Java parser using the grammar in ``grammars/foo.g4``, the command would be:

    $ make antlr4-python3 GRAMMAR-NAME=foo

The generated parser files will be put into the directory ``target/python3/<grammar-name>``. Any previous files with the same name as the generated parser files will be overwritten.

The Python 3-related Make commands will utilize a [virtual environment using venv](https://docs.python.org/3/library/venv.html) in the current directory. To clean up the virtual environment files from this project, run this command:

    $ make python3-clean
